function AnagramSolver(a, b) {
  return this.solve(a, b);

  if (!this instanceof AnagramSolver) {
    return new AnagramSolver(a, b);
  }
}

AnagramSolver.prototype.solve = function(a, b) {
  var aChars = a.split('');
  var bChars = b.split('');
  var aSorted = sortChars(aChars);
  var bSorted = sortChars(bChars);

  function sortChars(arr) {
    return arr.sort(function(a, b) {
      if (a < b) { return -1};
      if (a > b) { return 1};
      return 0;
    });
  }

  return aSorted.join('') == bSorted.join('');
}

module.exports = AnagramSolver;
