'use strict';

var assert = require('assert');
var AnagramSolver = require('../anagram');
var fs = require('fs');

describe('Anagram script', function() {
  var a = 'silent';
  var b = 'listen';
  var anagramSolver;

  beforeEach(function() {
    anagramSolver = new AnagramSolver(a, b);
  });

  it('determines whether a string is an anagram', function() {
    assert.equal(anagramSolver.solve(a, b), true);
  });
});
